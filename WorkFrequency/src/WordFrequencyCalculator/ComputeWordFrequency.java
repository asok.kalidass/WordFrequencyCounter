package WordFrequencyCalculator;
import java.io.*;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
/**
 * This class counts occurrences of words from a text file.
 *  Noteworthy Features: 
 * <ul>
 * <li>Identifies the top occurrence of words
 * <li>Identifies the least occurrence of words
 * <li>Generates .csv file listing all words by its rank
 * </ul>
 * @author Asok Kalidass Kalisamy (B00763356)
 *
 */
public class ComputeWordFrequency implements WordCount {
	//Declarations
	private Map<String, Integer> wordFrequency;
    /*
     * Constructor to initialize the commute frequency class
     */
	public ComputeWordFrequency(HashMap<String, Integer> wordFrequency) {
		this.wordFrequency =  wordFrequency;
	}
	 /*
     * Driver method to compute frequency of words 
     * @param args - command line inputs
     */
	public static void main(String[] args) {
		WordCount computeFreq = new ComputeWordFrequency(new HashMap<String, Integer>());
		List<Entry<String, Integer>> mostUsedWords;
		computeFreq.processFile("src//document.txt");
		mostUsedWords = computeFreq.highFrequencyWordsByDesc();
		computeFreq.leastFrequencyWordsByAsc();
		computeFreq.generateFile(mostUsedWords);
	}	
	/*
	 * Processes the text file
	 * @param filePath - path of the text file for which occurrences of words to be computed
	 * @throws File Not found exception if there is no file to process under src directory
	 * @throws Input Output exception if there any issue stream reader/writer
	 */
	@Override
	public void processFile(String filePath) {
		try {
			FileReader fileReader = new FileReader(filePath);
			@SuppressWarnings("resource")
			BufferedReader bufferReader = new BufferedReader(fileReader);
			String line = "";
			long lineLength = 0;
			StringBuilder file = new StringBuilder();
			//Reads the file line by line
			while ((line = bufferReader.readLine()) != null) {
				if (!(line.isEmpty())) {					
				file.append(line);
				file.append("\n");
				}
				lineLength++;
			}			
			System.out.println("Read length: " + lineLength);						
			String[] fileContents = file.toString().replaceAll("[^a-zA-Z]", "  ").toLowerCase().split("\\s");					
			//iterate over fileContents and insert/update hash map
			for (int i = 0; i < fileContents.length; i++) {
				if (!(fileContents[i].equalsIgnoreCase(""))) {					
				if (wordFrequency.containsKey(fileContents[i])) {
					wordFrequency.put(fileContents[i], wordFrequency.get(fileContents[i]) + 1);
				}
				else {					
					wordFrequency.put(fileContents[i], 1);
				}

			}
			}
			System.out.println("The total number of words are :" + wordFrequency.size());
		}
		catch (FileNotFoundException ex) {
        System.out.println("Exception at :" + ex.getMessage());
		}
		catch (IOException ex) {
			 System.out.println("Exception at :" + ex.getMessage());
		}
	}
	/*Computes the most occurring words 
	 * @returns List of most used words
	 * (non-Javadoc)
	 * @see IComputeWordFrequency#highFrequencyWordsByDesc(java.util.Map)
	 */
	@Override
	public List<Entry<String, Integer>> highFrequencyWordsByDesc() {

		List<Entry<String, Integer>> descSortedWordFreq = new ArrayList<>(this.wordFrequency.entrySet());
		int count = 0;
		//Comparator to get the sort by top occurring words
		Collections.sort(descSortedWordFreq, 
				new Comparator<Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> firstItem, Entry<String, Integer> secondItem) {
				return secondItem.getValue().compareTo(firstItem.getValue()); //to get descending
			}
		});
		System.out.println("The 20 most used Words in (words, frequency) format w.r.t its rank :");
		for (Entry<String, Integer> word : descSortedWordFreq) {
			if (count == 20) { break; }
			System.out.println(word.getKey() + ", " + word.getValue());
			count ++;
		}			
		return descSortedWordFreq;
	}
	/*Computes the least occurring words 
	 * @returns List of least used words
	 * (non-Javadoc)
	 * @see IComputeWordFrequency#leastFrequencyWordsByAsc(java.util.Map)
	 */
	@Override
	public List<Entry<String, Integer>> leastFrequencyWordsByAsc() {

		List<Entry<String, Integer>> ascSortedWordFreq = new ArrayList<>(this.wordFrequency.entrySet());
		int count = 0;
		//Comparator to get the sort by lest occurring words
		Collections.sort(ascSortedWordFreq, 
				new Comparator<Entry<String, Integer>>() {
			@Override
			public int compare(Entry<String, Integer> firstItem, Entry<String, Integer> secondItem) {
				return firstItem.getValue().compareTo(secondItem.getValue()); //to get descending
			}
		});
		System.out.println("The 20 least used Words in (words, frequency) format w.r.t its rank ::");
		for (Entry<String, Integer> word : ascSortedWordFreq) {
			if (count == 20) { break; }
			System.out.println(word.getKey() + ", " + word.getValue());
			count ++;
		}
		return ascSortedWordFreq;
	}
	/*
	 * Generates the list of words from documents and order it by rank
	 * @param - descSortedWordFreq - List of high frequency words
	 * @throws Input Output exception if there any issue stream reader/writer
	 */
	public void generateFile(List<Entry<String, Integer>> wordFrequency) {
		try {
			int rank = 1;				
			FileWriter  writer = new FileWriter("wordsFrquency.csv" , true);			
			PrintWriter write  = new PrintWriter(writer);
			for (Entry<String, Integer> word : wordFrequency) {								
			write.println(word.getKey() + ", " + word.getValue() + ", " + rank);
			rank++;			
			}
			write.close();
			write.flush();			
		}
		catch (IOException ex){
			 System.out.println("Exception at :" + ex.getMessage());
		}
	}   
}
