package WordFrequencyCalculator;
import java.util.List;
import java.util.Map.Entry;

/**
 * The WordCount Interface contains method to access the word frequency counter
 * @author Asok Kalidass Kalisamy (B00763356)
 *
 */
public interface WordCount {
	/*
	 * Processes the text file
	 * @param filePath - path of the text file for which occurrences of words to be computed
	 * @throws File Not found exception if there is no file to process under src directory
	 * @throws Input Output exception if there any issue stream reader/writer
	 */
	public void processFile(String filePath);
	
	/*Computes the most occurring words 
	 * @returns List of most used words
	*/
	public List<Entry<String, Integer>> highFrequencyWordsByDesc();
	
	/*Computes the least occurring words 
	 * @returns List of least used words
	 */
	public List<Entry<String, Integer>> leastFrequencyWordsByAsc();
	/*
	 * Generates the list of words from documents and order it by rank
	 * @param - descSortedWordFreq - List of high frequency words
	 * @throws Input Output exception if there any issue stream reader/writer
	 */
	public void generateFile(List<Entry<String, Integer>> wordFrequency);

	
}
