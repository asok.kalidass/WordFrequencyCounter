package WordFrequencyCalculator;
import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import org.junit.Test;
/**
 *  A Test class to validate the word frequency counter 
 * @author  Asok Kalidass Kalisamy (B00763356)
 *
 */
public class ComputeWordFrequencyTest {
    //Declaration
	WordCount wordFrequency = new ComputeWordFrequency(new HashMap<String, Integer>());
	/*
	 * A test to display the word frequency in .csv file
	 */
	@Test
	public void commputeRankTest() {
		List<Entry<String, Integer>> mostUsedWords;
		wordFrequency.processFile("src//document.txt");
		mostUsedWords = wordFrequency.highFrequencyWordsByDesc();
		wordFrequency.leastFrequencyWordsByAsc();
		wordFrequency.generateFile(mostUsedWords);
		//The file is generated with words along with its rank in project folder
		assertTrue(true);
	}
	/*
	 * A test to find the highest used word
	 */
	@Test
	public void highOccurentWordsTest() {
		int highFreqVal = 0;
		List<Entry<String, Integer>> mostUsedWords;
		wordFrequency.processFile("src//document.txt");
		mostUsedWords = wordFrequency.highFrequencyWordsByDesc();	
		for (Entry<String, Integer> words : mostUsedWords) {			
			highFreqVal = words.getValue().intValue();
			break;
		}
		assertEquals(6150, highFreqVal);
	}
	/*
	 * A test to find the second most used word
	 */
	@Test
	public void secondHighOccurentWordsTest() {
		int highFreqVal = 0;
		int count = 0;
		List<Entry<String, Integer>> mostUsedWords;
		wordFrequency.processFile("src//document.txt");
		mostUsedWords = wordFrequency.highFrequencyWordsByDesc();	
		for (Entry<String, Integer> words : mostUsedWords) {
			if (count == 2) { break; }
			highFreqVal = words.getValue().intValue();
			count++;
		}
		assertEquals(6001, highFreqVal);
	}
    /*
     * A test to display the twenty most used word
     */
	@Test
	public void topTwentyHighOccurentWordsTest() {
		wordFrequency.processFile("src//document.txt");
		wordFrequency.highFrequencyWordsByDesc();	
		//The output is displayed in console
	    assertTrue(true);
	}
	/*
	 * A test to find the least used word
	 */
	@Test
	public void leastOccurentWordsTest() {
		int leastFreqVal = 0;
		List<Entry<String, Integer>> mostUsedWords;
		wordFrequency.processFile("src//document.txt");
		mostUsedWords = wordFrequency.leastFrequencyWordsByAsc();	
		for (Entry<String, Integer> words : mostUsedWords) {			
			leastFreqVal = words.getValue().intValue();
			break;
		}
		assertEquals(1, leastFreqVal);
	}
	/*
	 * A test to find the least used word by its rank
	 */
	@Test
	public void secondLeastOccurentWordsTest() {
		String leastFreqWord = "";
		int count = 0;
		List<Entry<String, Integer>> mostUsedWords;
		wordFrequency.processFile("src//document.txt");
		mostUsedWords = wordFrequency.leastFrequencyWordsByAsc();	
		for (Entry<String, Integer> words : mostUsedWords) {
			if (count == 2) { break; }
			leastFreqWord = words.getKey().toString();
			count++;
		}
		assertEquals("shooting", leastFreqWord);
	}
	/*
	 * A test to find the twenty least used word by its rank
	 */
	@Test
	public void topTwentyLeastOccurentWordsTest() {
		wordFrequency.processFile("src//document.txt");
		wordFrequency.leastFrequencyWordsByAsc();	
		//The output is displayed in console
	    assertTrue(true);
	}
}
