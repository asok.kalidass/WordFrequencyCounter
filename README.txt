 
Pre - Requisite
   The input file should be in the path with name like - > src//document.txt

 WordFrequency Counter:

   Description: A program to count the frequency of words from a text file

   Files: 
    ComputeWordFrequency.java
    ComputeWordFrequencyTest.java
    WordCount.java

   Run instructions: 
     ComputeWordFrequency.java

   Test files:
    ComputeWordFrequencyTest.java

   Features:
   
   	- Identifies the most and least occurrence of words
   
   	- Generates the report with the word and its frequency in .csv format

   Zipf's Law:
        XY graph is plotted with log(rank) in X plane against log(frequency) in Y plane. Screen Shot uploaded for the same.

   Output :

        - The occourences of most and least frequency words will be displayed in console upon running ComputeWordFrequency.java

   Most Frequency words:
     the, 6150
     and, 6001
     of, 4848
     to, 4340
     in, 3079
     a, 2859
     that, 1882
     for, 1562
     or, 1266
     with, 1090
     is, 1088
     are, 1017
     on, 919
     health, 912
     as, 854
     students, 827
     soy, 807
     be, 797
     their, 775
     i, 738

   Least Frequency words:

     nicely, 1
     shooting, 1
     hall, 1
     naturalized, 1
     pretend, 1
     wreck, 1
     morton, 1
     investment, 1
     vegetarianism, 1
     cds, 1
     rounded, 1
     hang,  1
     fontana, 1
     transitional, 1
     dieticians, 1
     resuscitation, 1
     gripping, 1
     readers, 1
     obvious, 1
     regain, 1